setTimeout(function ()
{
    var resultsPage = document.getElementById('resultListControl');
    if (resultsPage)
    {
        var locationToggle = document.getElementById('multiSelectCluster_LocationLibraryTrigger');
        if ((resultsPage.innerHTML.indexOf('Los Rios Online') > -1) && (locationToggle))
        {
            var locationState = locationToggle.getAttribute('title');
            console.log(document.querySelector('.page-title').textContent);
            if ((locationState.indexOf('Show this area') > -1) && (document.querySelector('.page-title').textContent.indexOf('Results: 1 -') > -1))
            {
                locationToggle.removeEventListener('click', ga);
                locationToggle.click();
            }
        }
    }
}, 800);